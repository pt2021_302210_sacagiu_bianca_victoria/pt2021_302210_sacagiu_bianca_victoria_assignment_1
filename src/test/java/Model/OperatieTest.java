package Model;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class OperatieTest {

    @Test
    public void testAdunare() throws Exceptie {
        Polinom polinom1 = new Polinom();
        Polinom polinom2 = new Polinom();
        polinom1 = Operatie.verificare("x^2+2x+3");
        polinom2 = Operatie.verificare("x^2+1");
        Polinom polinomRez = new Polinom();
        polinomRez = Operatie.adunare(polinom1, polinom2);
        assertEquals("2x^2+2x+4", Operatie.toString(polinomRez));
    }

    @Test
    public void testScadere() throws Exceptie {
        Polinom polinom1 = new Polinom();
        Polinom polinom2 = new Polinom();
        polinom1 = Operatie.verificare("x^2+2x+3");
        polinom2 = Operatie.verificare("x^2+1");
        Polinom polinomRez = new Polinom();
        polinomRez = Operatie.scadere(polinom1, polinom2);
        assertEquals("2x+2", Operatie.toString(polinomRez));
    }

    @Test
    public void testDerivare() throws Exceptie {
        Polinom polinom1 = new Polinom();
        polinom1 = Operatie.verificare("x^2+2x+3");
        Polinom polinomRez = new Polinom();
        polinomRez = Operatie.derivare(polinom1);
        assertEquals("2x+2", Operatie.toString(polinomRez));
    }

    @Test
    public void testIntegrare() throws Exceptie {
        Polinom polinom1 = new Polinom();
        polinom1 = Operatie.verificare("x^2+2x+3");
        Polinom polinomRez = new Polinom();
        polinomRez = Operatie.integrare(polinom1);
        assertEquals("1/3x^3+x^2+3x", Operatie.toString(polinomRez));
    }

    @Test
    public void testInmultire() throws Exceptie {
        Polinom polinom1 = new Polinom();
        Polinom polinom2 = new Polinom();
        polinom1 = Operatie.verificare("x^2+2x+3");
        polinom2 = Operatie.verificare("x^2+1");
        Polinom polinomRez = new Polinom();
        polinomRez = Operatie.inmultire(polinom1, polinom2);
        assertEquals("x^4+2x^3+4x^2+2x+3", Operatie.toString(polinomRez));
    }

    @Test
    public void testImpartire() throws Exceptie {
        Polinom polinom1 = new Polinom();
        Polinom polinom2 = new Polinom();
        polinom1 = Operatie.verificare("x^3-2x^2+6x-5");
        polinom2 = Operatie.verificare("x^2-1");
        String polinomRez = Operatie.impartire(polinom1, polinom2);
        assertEquals("Cat: x-2 Rest: 7x-7", polinomRez);
    }

    public void method() {
    }
}