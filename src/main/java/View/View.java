package View;
import Model.Polinom;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.*;
import javax.swing.JButton;

public class View {
    JLabel etPol1;
    JTextField txtPol1;
    JLabel etPol2;
    JTextField txtPol2;
    JLabel etRez;
    JLabel etRez2;
    JButton btnAdunare;
    JButton btnScadere;
    JButton btnInmultire;
    JButton btnImpartire;
    JButton btnDerivare;
    JButton btnIntegrare;
    JFrame frame;
    public View(){
        frame = new JFrame("Calculator de Polinoame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(100, 100, 650, 400);
        frame.setLayout(null);
        etPol1 = new JLabel("Introduceti primul polinom:");
        etPol1.setFont(new Font("Arial", Font.BOLD, 20));
        etPol1.setBounds(40, 22, 300, 100);
        frame.add(etPol1);
        txtPol1 = new JTextField("");
        txtPol1.setBounds(40, 92, 270, 30);
        frame.add(txtPol1);
        etPol2 = new JLabel("Introduceti al doilea polinom:");
        etPol2.setFont(new Font("Arial", Font.BOLD, 20));
        etPol2.setBounds(40, 110, 300, 100);
        frame.add(etPol2);
        txtPol2 = new JTextField("");
        txtPol2.setBounds(40, 180, 270, 30);
        frame.add(txtPol2);
        etRez = new JLabel("Rezultatul este:");
        etRez.setFont(new Font("Arial", Font.BOLD, 20));
        etRez.setBounds(40, 240, 300, 100);
        frame.add(etRez);
        etRez2 = new JLabel("");
        etRez2.setFont(new Font("Arial", Font.BOLD, 20));
        etRez2.setForeground(Color.RED);
        etRez2.setBounds(195, 240, 400, 100);
        frame.add(etRez2);
        btnAdunare = new JButton("+");
        btnAdunare.setBounds(390, 60, 100, 40);
        frame.add(btnAdunare);
        btnScadere = new JButton("-");
        btnScadere.setBounds(500, 60, 100, 40);
        frame.add(btnScadere);
        btnInmultire = new JButton("*");
        btnInmultire.setBounds(390, 130, 100, 40);
        frame.add(btnInmultire);
        btnImpartire = new JButton("/");
        btnImpartire.setBounds(500, 130, 100, 40);
        frame.add(btnImpartire);
        btnDerivare = new JButton("Derivare");
        btnDerivare.setBounds(390, 200, 100, 40);
        frame.add(btnDerivare);
        btnIntegrare = new JButton("Integrare");
        btnIntegrare.setBounds(500, 200, 100, 40);
        frame.add(btnIntegrare);
        frame.setVisible(true);
    }
    public String getTxtPol1() {
        return txtPol1.getText();
    }
    public String getTxtPol2() {
        return txtPol2.getText();
    }
    public void setEtRez2(String e) {
        etRez2.setText(e);
    }
    public JButton getBtnAdunare() {
        return btnAdunare;
    }
    public JButton getBtnDerivare() {
        return btnDerivare;
    }
    public JButton getBtnImpartire() {
        return btnImpartire;
    }
    public JButton getBtnInmultire() {
        return btnInmultire;
    }
    public JButton getBtnIntegrare() {
        return btnIntegrare;
    }
    public JButton getBtnScadere() {
        return btnScadere;
    }
}