package Model;

import javax.swing.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Operatie<polinomRez> {

    public static Polinom verificare(String s) throws Exceptie {
        Pattern pattern = Pattern.compile("([+-]?[^-+]+)");
        Matcher matcher = pattern.matcher(s);
        Polinom polinom = new Polinom();
        Polinom polinomPrelucrat = new Polinom();
        if (s.equals("") || s.equals(" ")) {
            throw new Exceptie("Polinomul introdus este invalid");
        }
        Pattern pattern2 = Pattern.compile(("[+-]?[0-9]*([Xx])?\\^?[0-9]*"));
        Matcher matcher2;
        while (matcher.find()) {
            matcher2 = pattern2.matcher(matcher.group(1));
            if (!matcher2.matches())
                throw new Exceptie("Polinomul introdus este invalid");
            //System.out.println(matcher.group(1));
            polinomPrelucrat.addMonom(citirePolinom(matcher.group(1)));
        }
        return polinomPrelucrat;
    }

    private static Monom citirePolinom(String monom) {
        int grad = 0;
        int coeficient = 0;
        int i = 0;
        int gradZero = 1;
        int esteZero=0;
        String semn = "+";
        while (monom.length() > i && monom.charAt(i) != 'x' && monom.charAt(i) != 'X') {
            if (Character.isDigit(monom.charAt(i))) {
                if (i != 0) {
                    if (monom.charAt(i - 1) == '-') {
                        semn = "-";
                    }
                }
                if (semn.equals("-")) {
                    coeficient = coeficient * 10 - Character.getNumericValue(monom.charAt(i));
                } else {
                    coeficient = coeficient * 10 + Character.getNumericValue(monom.charAt(i));
                }
            }
            i++;
        }
        while (monom.length() > i) {
            if (Character.isDigit(monom.charAt(i))) {
                grad = grad * 10 + Character.getNumericValue(monom.charAt(i));
            }
            if (monom.charAt(i) == 'x' || monom.charAt(i) == 'X') {
                gradZero = 0;
            }
            i++;
        }
        if (grad == 0 && gradZero == 0) {
            grad = 1;
        }
        if (coeficient == 0) {
            if (monom.charAt(0) == '-')
                coeficient = -1;
            else
                coeficient=1;
        }
        Monom m = new Monom(coeficient, grad, 1);
        return m;
    }

    public static Polinom sortare(Polinom polinom) {
        for (int i = 0; i < polinom.getPolinom().size() - 1; i++) {
            for (int j = i + 1; j < polinom.getPolinom().size(); j++)
                if (polinom.getPolinom().get(i).getGrad() < (polinom.getPolinom().get(j).getGrad())) {
                    Monom aux;
                    aux = polinom.getPolinom().get(i);
                    polinom.getPolinom().set(i, polinom.getPolinom().get(j));
                    polinom.getPolinom().set(j, aux);
                }
        }
        return polinom;
    }

    public static String toString(Polinom polinom) {
        String s = "";
        for (int i = 0; i < polinom.getPolinom().size(); i++) {
            if (polinom.getPolinom().get(i).getGrad() == 0) {
                if (polinom.getPolinom().get(i).getCoeficient() != 1 && polinom.getPolinom().get(i).getCoeficient() != -1) {
                    if (polinom.getPolinom().get(i).getCoeficient() > 0) {
                        if (polinom.getPolinom().get(i).getCoefNumitor() == 0 || polinom.getPolinom().get(i).getCoefNumitor() == 1)
                            s = s + "+" + polinom.getPolinom().get(i).getCoeficient();
                        else
                            s = s + "+" + polinom.getPolinom().get(i).getCoeficient() + "/" + polinom.getPolinom().get(i).getCoefNumitor();
                    } else if (polinom.getPolinom().get(i).getCoeficient() < 0) {
                        if (polinom.getPolinom().get(i).getCoefNumitor() == 0 || polinom.getPolinom().get(i).getCoefNumitor() == 1)
                            s = s + polinom.getPolinom().get(i).getCoeficient();
                        else
                            s = s + polinom.getPolinom().get(i).getCoeficient() + "/" + polinom.getPolinom().get(i).getCoefNumitor();
                    }
                } else if (polinom.getPolinom().get(i).getCoeficient() == 1) {
                    if (polinom.getPolinom().get(i).getCoefNumitor() == 0 || polinom.getPolinom().get(i).getCoefNumitor() == 1)
                        s = s + "+" + polinom.getPolinom().get(i).getCoeficient();
                    else
                        s = s + "+" + polinom.getPolinom().get(i).getCoeficient() + "/" + polinom.getPolinom().get(i).getCoefNumitor();
                } else if (polinom.getPolinom().get(i).getCoeficient() == -1) {
                    if (polinom.getPolinom().get(i).getCoefNumitor() == 0 || polinom.getPolinom().get(i).getCoefNumitor() == 1)
                        s = s + polinom.getPolinom().get(i).getCoeficient();
                    else
                        s = s + polinom.getPolinom().get(i).getCoeficient() + "/" + polinom.getPolinom().get(i).getCoefNumitor();
                }
            } else if (polinom.getPolinom().get(i).getGrad() == 1) {
                if (polinom.getPolinom().get(i).getCoeficient() == 1) {
                    if (polinom.getPolinom().get(i).getCoefNumitor() == 0 || polinom.getPolinom().get(i).getCoefNumitor() == 1)
                        s = s + "+" + "x";
                    else
                        s = s + "+" + "1/" + polinom.getPolinom().get(i).getCoefNumitor() + "x";
                } else if (polinom.getPolinom().get(i).getCoeficient() == -1) {
                    if (polinom.getPolinom().get(i).getCoefNumitor() == 0 || polinom.getPolinom().get(i).getCoefNumitor() == 1)
                        s = s + "-" + "x";
                    else
                        s = s + "-" + "1/" + polinom.getPolinom().get(i).getCoefNumitor() + "x";
                } else if (polinom.getPolinom().get(i).getCoeficient() > 1) {
                    if (polinom.getPolinom().get(i).getCoefNumitor() == 0 || polinom.getPolinom().get(i).getCoefNumitor() == 1)
                        s = s + "+" + polinom.getPolinom().get(i).getCoeficient() + "x";
                    else
                        s = s + "+" + polinom.getPolinom().get(i).getCoeficient() + "/" + polinom.getPolinom().get(i).getCoefNumitor() + "x";
                } else if (polinom.getPolinom().get(i).getCoeficient() < -1) {
                    if (polinom.getPolinom().get(i).getCoefNumitor() == 0 || polinom.getPolinom().get(i).getCoefNumitor() == 1)
                        s = s + polinom.getPolinom().get(i).getCoeficient() + "x";
                    else
                        s = s + polinom.getPolinom().get(i).getCoeficient() + "/" + polinom.getPolinom().get(i).getCoefNumitor() + "x";
                }
            } else {
                if (polinom.getPolinom().get(i).getCoeficient() == 1) {
                    if (polinom.getPolinom().get(i).getCoefNumitor() == 0 || polinom.getPolinom().get(i).getCoefNumitor() == 1)
                        s = s + "+x^" + polinom.getPolinom().get(i).getGrad();
                    else
                        s = s + "1/" + polinom.getPolinom().get(i).getCoefNumitor() + "x^" + polinom.getPolinom().get(i).getGrad();
                } else if (polinom.getPolinom().get(i).getCoeficient() == -1) {
                    if (polinom.getPolinom().get(i).getCoefNumitor() == 0 || polinom.getPolinom().get(i).getCoefNumitor() == 1)
                        s = s + "-x^" + polinom.getPolinom().get(i).getGrad();
                    else
                        s = s + "-1/" + polinom.getPolinom().get(i).getCoefNumitor() + "x^" + polinom.getPolinom().get(i).getGrad();
                } else if (polinom.getPolinom().get(i).getCoeficient() > 1) {
                    if (polinom.getPolinom().get(i).getCoefNumitor() == 0 || polinom.getPolinom().get(i).getCoefNumitor() == 1)
                        s = s + "+" + polinom.getPolinom().get(i).getCoeficient() + "x^" + polinom.getPolinom().get(i).getGrad();
                    else
                        s = s + "+" + polinom.getPolinom().get(i).getCoeficient() + "/" + polinom.getPolinom().get(i).getCoefNumitor() + "x^" + polinom.getPolinom().get(i).getGrad();
                } else if (polinom.getPolinom().get(i).getCoeficient() < -1) {
                    if (polinom.getPolinom().get(i).getCoefNumitor() == 0 || polinom.getPolinom().get(i).getCoefNumitor() == 1)
                        s = s + polinom.getPolinom().get(i).getCoeficient() + "x^" + polinom.getPolinom().get(i).getGrad();
                    else
                        s = s + polinom.getPolinom().get(i).getCoeficient() + "/" + polinom.getPolinom().get(i).getCoeficient() + "x^" + polinom.getPolinom().get(i).getGrad();
                }
            }
        }
        if(s.isEmpty()==false) {
            if (s.charAt(0) == '+') {
                s = s.substring(1);
            }
        }
        return s;
    }

    public static Polinom adunare(Polinom polinom1, Polinom polinom2) {
        Polinom polinomRez = new Polinom();
        int i = 0;
        int j = 0;
        while (i < polinom1.getPolinom().size() && j < polinom2.getPolinom().size()) {
            if (polinom1.getPolinom().get(i).getGrad() > polinom2.getPolinom().get(j).getGrad()) {
                Monom m = new Monom(polinom1.getPolinom().get(i).getCoeficient(),
                        polinom1.getPolinom().get(i).getGrad(), 1);
                polinomRez.addMonom(m);
                i++;
            } else if (polinom1.getPolinom().get(i).getGrad() < polinom2.getPolinom().get(j).getGrad()) {
                Monom m = new Monom(polinom2.getPolinom().get(j).getCoeficient(),
                        polinom2.getPolinom().get(j).getGrad(), 1);
                polinomRez.addMonom(m);
                j++;
            } else {
                Monom m = new Monom(polinom1.getPolinom().get(i).getCoeficient() +
                        polinom2.getPolinom().get(j).getCoeficient(), polinom1.getPolinom().get(i).getGrad(), 1);
                polinomRez.addMonom(m);
                i++;
                j++;
            }
        }
        while (i < polinom1.getPolinom().size()) {
            Monom m = new Monom(polinom1.getPolinom().get(i).getCoeficient(),
                    polinom1.getPolinom().get(i).getGrad(), 1);
            polinomRez.addMonom(m);
            i++;
        }
        while (j < polinom2.getPolinom().size()) {
            Monom m = new Monom(polinom2.getPolinom().get(j).getCoeficient(),
                    polinom2.getPolinom().get(j).getGrad(), 1);
            polinomRez.addMonom(m);
            j++;
        }
        return polinomRez;
    }

    public static Polinom scadere(Polinom polinom1, Polinom polinom2) {
        Polinom polinomRez = new Polinom();
        int i = 0;
        int j = 0;
        while (i < polinom1.getPolinom().size() && j < polinom2.getPolinom().size()) {
            if (polinom1.getPolinom().get(i).getGrad() > polinom2.getPolinom().get(j).getGrad()) {
                Monom m = new Monom(polinom1.getPolinom().get(i).getCoeficient(),
                        polinom1.getPolinom().get(i).getGrad(), 1);
                polinomRez.addMonom(m);
                i++;
            } else if (polinom1.getPolinom().get(i).getGrad() < polinom2.getPolinom().get(j).getGrad()) {
                Monom m = new Monom(-polinom2.getPolinom().get(j).getCoeficient(),
                        polinom2.getPolinom().get(j).getGrad(), 1);
                polinomRez.addMonom(m);
                j++;
            } else {
                Monom m = new Monom(polinom1.getPolinom().get(i).getCoeficient() -
                        polinom2.getPolinom().get(j).getCoeficient(), polinom1.getPolinom().get(i).getGrad(), 1);
                polinomRez.addMonom(m);
                i++;
                j++;
            }
        }
        while (i < polinom1.getPolinom().size()) {
            Monom m = new Monom(polinom1.getPolinom().get(i).getCoeficient(),
                    polinom1.getPolinom().get(i).getGrad(), 1);
            polinomRez.addMonom(m);
            i++;
        }
        while (j < polinom2.getPolinom().size()) {
            Monom m = new Monom(-polinom2.getPolinom().get(j).getCoeficient(),
                    polinom2.getPolinom().get(j).getGrad(), 1);
            polinomRez.addMonom(m);
            j++;
        }
        return polinomRez;
    }

    public static Polinom derivare(Polinom polinom) {
        Polinom polinomRez = new Polinom();
        for (int i = 0; i < polinom.getPolinom().size(); i++) {
            Monom m = new Monom(polinom.getPolinom().get(i).getCoeficient() * polinom.getPolinom().get(i).getGrad(), polinom.getPolinom().get(i).getGrad() - 1, 1);
            polinomRez.addMonom(m);
        }
        return polinomRez;
    }
    public static int cmmdc (int nr1, int nr2){
        while(nr1 != nr2)
        {
            if(nr1 > nr2)
                nr1 = nr1 - nr2;
            else
                nr2 = nr2 - nr1;
        }
        return nr1;
    }
    public static Polinom integrare(Polinom polinom) {
        Polinom polinomRez = new Polinom();
        for (int i = 0; i < polinom.getPolinom().size(); i++) {
            Monom m = new Monom(polinom.getPolinom().get(i).getCoeficient() / cmmdc(polinom.getPolinom().get(i).getCoeficient(),
                    polinom.getPolinom().get(i).getGrad() + 1), polinom.getPolinom().get(i).getGrad() + 1,
                    (polinom.getPolinom().get(i).getGrad() + 1) / cmmdc(polinom.getPolinom().get(i).getCoeficient(), polinom.getPolinom().get(i).getGrad() + 1));
            polinomRez.addMonom(m);
        }
        return polinomRez;
    }

    public static Polinom inmultire(Polinom polinom1, Polinom polinom2){
        Polinom polinomRez = new Polinom();
        for(int i = 0; i < polinom1.getPolinom().size(); i ++){
            for(int j = 0; j < polinom2.getPolinom().size(); j ++) {
                Monom m = new Monom(polinom1.getPolinom().get(i).getCoeficient() * polinom2.getPolinom().get(j).getCoeficient(),
                        polinom1.getPolinom().get(i).getGrad() + polinom2.getPolinom().get(j).getGrad(), 1);
                polinomRez.addMonom(m);
            }
        }
        for(int i = 0; i < polinomRez.getPolinom().size(); i ++) {
            for (int j = 0; j < polinomRez.getPolinom().size(); j++) {
                if (i != j) {
                    if (polinomRez.getPolinom().get(i).getGrad() == polinomRez.getPolinom().get(j).getGrad()) {

                        Monom m = new Monom(polinomRez.getPolinom().get(i).getCoeficient() + polinomRez.getPolinom().get(j).getCoeficient(),
                                polinomRez.getPolinom().get(i).getGrad(), 1);
                        if(i < j){
                            polinomRez.getPolinom().remove(i);
                            polinomRez.getPolinom().remove(j-1);
                            polinomRez.addMonom(m);
                        }
                        else{
                            polinomRez.getPolinom().remove(i);
                            polinomRez.getPolinom().remove(j);
                            polinomRez.addMonom(m);
                        }
                    }
                }
            }
        }
        Operatie.sortare(polinomRez);
        return polinomRez;
    }


    private static Polinom clean(Polinom polinom){
        for(int i = 0; i < polinom.getPolinom().size(); i ++){
            if(polinom.getPolinom().get(i).getCoeficient() == 0){
                polinom.getPolinom().remove(i);
            }
        }
        return polinom;
    }
    private static Polinom inmultireImp(Monom mon, Polinom pol) {
        Polinom polinomRez = new Polinom();
        for (Monom monom : pol.getPolinom()) {
            polinomRez.addMonom(new Monom(mon.getCoeficient() * monom.getCoeficient(), mon.getGrad() + monom.getGrad(), 1));
        }
        return polinomRez;
    }
    private static String impartire2(Polinom polinom1, Polinom polinom2){
        Polinom cat = new Polinom();

        while(polinom1.getPolinom().get(0).getGrad() >= polinom2.getPolinom().get(0).getGrad() && polinom1.getPolinom().isEmpty() == false) {
            float s1 = (float) polinom1.getPolinom().get(0).getCoeficient() / polinom2.getPolinom().get(0).getCoeficient();
            int s2 = polinom1.getPolinom().get(0).getCoeficient() / polinom2.getPolinom().get(0).getCoeficient();
            if (s1 != s2) {
                JOptionPane.showMessageDialog(null,
                        "Rezultatul are coeficienti reali",
                        "Eroare!",
                        JOptionPane.ERROR_MESSAGE);
                return "";
            }
            if (polinom1.getPolinom().get(0).getCoeficient() == 0 || polinom2.getPolinom().get(0).getCoeficient() == 0){
                JOptionPane.showMessageDialog(null,
                        "Impartirea cu 0 nu are sens matematic",
                        "Eroare!",
                        JOptionPane.ERROR_MESSAGE);
                return "";
            }
            Monom aux=new Monom(polinom1.getPolinom().get(0).getCoeficient()/polinom2.getPolinom().get(0).getCoeficient(),polinom1.getPolinom().get(0).getGrad()-polinom2.getPolinom().get(0).getGrad(),1);
            cat.addMonom(aux);
            Polinom aux2=inmultireImp(aux,polinom2);
            Polinom aux3=scadere(polinom1,aux2);
            polinom1=aux3;
            polinom1 = clean(polinom1);
            polinom2 = clean(polinom2);
            if(polinom1.getPolinom().isEmpty()) {
                break;
            }
        }
        if(polinom1.getPolinom().isEmpty()==true)
            return ("Cat: "+Operatie.toString(cat)+" Rest: 0");
        else
            return ("Cat: "+Operatie.toString(cat)+" Rest: "+Operatie.toString(polinom1));

    }

    public static String impartire(Polinom polinom1, Polinom polinom2){
        Operatie.sortare(polinom1);
        Operatie.sortare(polinom2);
        System.out.println(polinom1.getPolinom().get(0).getCoeficient());
        if(polinom1.getPolinom().get(0).getGrad() >= polinom2.getPolinom().get(0).getGrad()){
            return impartire2(polinom1, polinom2);
        }
        else{
            return impartire2(polinom2, polinom1);
        }

    }



}
