package Model;

import java.util.ArrayList;

public class Polinom {
    private ArrayList<Monom> polinom = new ArrayList<Monom>();

    public void addMonom(Monom monom){
        this.polinom.add(monom);
    }

    public ArrayList<Monom> getPolinom() {
        return polinom;
    }
}
