package Model;

public class Monom {
    private int coeficient;
    private int grad;
    private int coefNumitor;
    public Monom(int coef, int grad, int coefNumitor){
        this.grad = grad;
        this.coeficient = coef;
        this.coefNumitor = coefNumitor;

    }
    public Monom(){
        this.grad = grad;
        this.coeficient = coeficient;
        this.coefNumitor = coefNumitor;
    }
    public int getCoeficient(){
        return this.coeficient;
    }
    public int getGrad() {
        return grad;
    }

    public int getCoefNumitor() {
        return coefNumitor;
    }

    public void setCoeficient(int coeficient) {
        this.coeficient = coeficient;
    }

    public void setGrad(int grad) {
        this.grad = grad;
    }
}
