package Main;

import Controller.Controller;
import Model.Exceptie;
import Model.Operatie;
import View.View;


public class MainClass {
    public static void main(String[] args) throws Exceptie {
        View view = new View();
        Operatie operatie = new Operatie();
        Controller controller = new Controller(operatie, view);
    }
}