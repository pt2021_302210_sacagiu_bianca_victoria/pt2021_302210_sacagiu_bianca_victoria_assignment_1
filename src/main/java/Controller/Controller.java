package Controller;

import Model.Exceptie;
import Model.Operatie;
import Model.Polinom;
import View.View;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Controller {
    View view;
    Operatie operatie;
    public Controller(Operatie operatie, View view) {
        view.getBtnAdunare().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom p1 = new Polinom();
                Polinom p2 = new Polinom();
                try {
                    p1 = Operatie.verificare(view.getTxtPol1());
                    p2 = Operatie.verificare(view.getTxtPol2());
                } catch (Exceptie exceptie) {
                    JOptionPane.showMessageDialog(null, exceptie.getMessage(), "Eroare!",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String s;
                Polinom p = new Polinom();
                p = Operatie.sortare(Operatie.adunare(p1, p2));
                s = Operatie.toString(p);
                view.setEtRez2(s);
            }
        });

        view.getBtnScadere().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom p1 = new Polinom();
                Polinom p2 = new Polinom();
                try {
                    p1 = Operatie.verificare(view.getTxtPol1());
                    p2 = Operatie.verificare(view.getTxtPol2());
                } catch (Exceptie exceptie) {
                    JOptionPane.showMessageDialog(null, exceptie.getMessage(), "Eroare!",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String s;
                Polinom p = new Polinom();
                p = Operatie.sortare(Operatie.scadere(p1, p2));
                s = Operatie.toString(p);
                view.setEtRez2(s);
            }
        });

        view.getBtnDerivare().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom p1 = new Polinom();
                Polinom p2 = new Polinom();
                try {
                    p1 = Operatie.verificare(view.getTxtPol1());
                    p2 = Operatie.verificare(view.getTxtPol2());
                } catch (Exceptie exceptie) {
                    JOptionPane.showMessageDialog(null, exceptie.getMessage(), "Eroare!",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String s;
                Polinom p = new Polinom();
                p = Operatie.sortare(Operatie.derivare(p1));
                s = Operatie.toString(p);
                view.setEtRez2(s);
            }
        });

        view.getBtnInmultire().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom p1 = new Polinom();
                Polinom p2 = new Polinom();
                try {
                    p1 = Operatie.verificare(view.getTxtPol1());
                    p2 = Operatie.verificare(view.getTxtPol2());
                } catch (Exceptie exceptie) {
                    JOptionPane.showMessageDialog(null, exceptie.getMessage(), "Eroare!",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String s;
                Polinom p = new Polinom();
                p = Operatie.sortare(Operatie.inmultire(p1, p2));
                s = Operatie.toString(p);
                view.setEtRez2(s);
            }
        });

        view.getBtnIntegrare().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom p1 = new Polinom();
                Polinom p2 = new Polinom();
                try {
                    p1 = Operatie.verificare(view.getTxtPol1());
                    p2 = Operatie.verificare(view.getTxtPol2());
                } catch (Exceptie exceptie) {
                    JOptionPane.showMessageDialog(null, exceptie.getMessage(), "Eroare!",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String s;
                Polinom p = new Polinom();
                p = Operatie.sortare(Operatie.integrare(p1));
                s = Operatie.toString(p);
                view.setEtRez2(s);
            }
        });

        view.getBtnImpartire().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Polinom p1 = new Polinom();
                Polinom p2 = new Polinom();
                try {
                    p1 = Operatie.verificare(view.getTxtPol1());
                    p2 = Operatie.verificare(view.getTxtPol2());
                } catch (Exceptie exceptie) {
                    JOptionPane.showMessageDialog(null, exceptie.getMessage(), "Eroare!",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String s;
                s = Operatie.impartire(p1, p2);
                view.setEtRez2(s);
            }
        });
    }
}
